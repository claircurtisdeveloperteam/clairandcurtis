// affix
$('.navbar').affix({
      offset: {
        top: $('.cover').height()
      }
});

// smooth scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// close collapse menu after click
$('.navbar-nav li a').on('click', function(e) {
  if( $(e.target).is('a') ) {
        $('.navbar-collapse.in').collapse('hide');
    }
});

//mixItUp
$(function(){
  $('#news-slider').mixItUp({
    animation: {
      duration: 300,
      effects: 'fade translateX(-100%) stagger(34ms)',
      easing: 'ease'
    },
    pagination: {
      limit: 2,
      loop: true,
      prevButtonHTML: '<img src="./images/arrow_left.png" alt="" />',
      nextButtonHTML: '<img src="./images/arrow_right.png" alt="" />'
    },
    selectors: {
      filter: 'none',
      pagersWrapper: '.news-pager'
    },
    controls: {
      enable: true
    }
  });
});

$(function(){
  $('#portfolio').mixItUp({
    animation: {
      duration: 300,
      effects: 'fade translateX(-10%) stagger(34ms)',
      easing: 'ease'
    },
    pagination: {
      limit: 6,
      prevButtonHTML: '<img src="./images/arrow_left.png" alt="" />',
      nextButtonHTML: '<img src="./images/arrow_right.png" alt="" />'
    },
    selectors: {
      filter: '.filter',
      pagersWrapper: '.works-pager'
    },
    controls: {
      enable: true
    }
  });
});

$(function(){
  $('#concept-slider').mixItUp({
    animation: {
      duration: 300,
      effects: 'fade translateX(-100%) stagger(34ms)',
      easing: 'ease'
    },
    pagination: {
      limit: 4,
      loop: true,
      prevButtonHTML: '<img src="./images/arrow_left.png" alt="" />',
      nextButtonHTML: '<img src="./images/arrow_right.png" alt="" />'
    },
    selectors: {
      filter: 'none',
      pagersWrapper: '.concept-pager'
    },
    controls: {
      enable: true
    }
  });
});

$(function(){
  $('#process-slider').mixItUp({
    animation: {
      duration: 300,
      effects: 'fade translateX(-100%)',
      easing: 'ease'
    },
    pagination: {
      limit: 1,
      loop: true,
      prevButtonHTML: '<img src="./images/arrow_left.png" alt="" />',
      nextButtonHTML: '<img src="./images/arrow_right.png" alt="" />'
    },
    selectors: {
      filter: 'none',
      pagersWrapper: '.process-pager'
    },
    controls: {
      enable: true
    }
  });
});

$(function(){
  $('#portfolio-slider').mixItUp({
    animation: {
      duration: 300,
      effects: 'fade translateX(-100%)',
      easing: 'ease'
    },
    pagination: {
      limit: 1,
      loop: true,
      prevButtonHTML: '<img src="./images/arrow_left.png" alt="" />',
      nextButtonHTML: '<img src="./images/arrow_right.png" alt="" />'
    },
    selectors: {
      filter: 'none',
      pagersWrapper: '.portfolio-pager'
    },
    controls: {
      enable: true
    }
  });
});